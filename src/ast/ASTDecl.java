package ast;

import java.util.LinkedList;
import java.util.List;

import compiler.CodeBlock;
import util.Binding;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTDecl implements ASTNode {
	
	LinkedList<Binding> decl;
	ASTNode expr;

    public ASTDecl(LinkedList<Binding> decl, ASTNode expr)
    {
		this.decl = decl; 
		this.expr = expr;
    }

    public String toString() {
    	String s = "";
    	for(int i=0; i<decl.size(); i++){
    	
    		s += decl.get(i).getId() + " = " + decl.get(i).getExpr().toString();
    	}
    	return "decl " + s + " in " + expr.toString() + " end";
    }
    
	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		int value;
		Environment<Integer> newEnv = env.beginScope();	
		for(int i=0; i<decl.size();i++){
		int idValue = decl.get(i).getExpr().eval(env);		
		newEnv.assoc(decl.get(i).getId(), idValue);
		}
		value = expr.eval(newEnv);
		newEnv.endScope();

		return value;
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}


}


package ast;


import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;


public class ASTAdd implements ASTNode {

	ASTNode left, right;

	public ASTAdd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return left.eval(env) + right.eval(env);
	}
	
	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		code.emit_add();
	}

	@Override
	public String toString() {
		return left.toString() + " + " + right.toString();
	}
}

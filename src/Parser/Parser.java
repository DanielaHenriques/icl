/* Generated By:JavaCC: Do not edit this line. Parser.java */
package parser;
import ast.*;
import util.*;
import java.util.LinkedList;

public class Parser implements ParserConstants {

  final public ASTNode Start() throws ParseException {
  ASTNode e;
    e = E();
    jj_consume_token(EL);
                   {if (true) return e;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode E() throws ParseException {
  ASTNode e;
    e = disj();
                 {if (true) return e;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode disj() throws ParseException {
  ASTNode e1,e2;
    e1 = conj();
    label_1:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case OR:
        ;
        break;
      default:
        jj_la1[0] = jj_gen;
        break label_1;
      }
      jj_consume_token(OR);
      e2 = conj();
                        e1=new ASTOr(e1,e2);
    }
          {if (true) return e1;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode conj() throws ParseException {
  ASTNode e1,e2;
    e1 = Equality();
    label_2:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case AND:
        ;
        break;
      default:
        jj_la1[1] = jj_gen;
        break label_2;
      }
      jj_consume_token(AND);
      e2 = Equality();
                             e1=new ASTAnd(e1,e2);
    }
          {if (true) return e1;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode Equality() throws ParseException {
 ASTNode e1, e2;
    e1 = Relational();
    label_3:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case DIF:
      case EQUAL:
        ;
        break;
      default:
        jj_la1[2] = jj_gen;
        break label_3;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case EQUAL:
        jj_consume_token(EQUAL);
        e2 = Relational();
                               e1=new ASTEqual(e1,e2);
        break;
      case DIF:
        jj_consume_token(DIF);
        e2 = Relational();
                           e1=new ASTDif(e1,e2);
        break;
      default:
        jj_la1[3] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
    {if (true) return e1;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode Relational() throws ParseException {
 ASTNode e1, e2;
    e1 = Exp();
    label_4:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LESS:
      case HIGHER:
      case HIEQ:
      case LOEQ:
        ;
        break;
      default:
        jj_la1[4] = jj_gen;
        break label_4;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case HIEQ:
        jj_consume_token(HIEQ);
        e2 = Exp();
                      e1=new ASTHieq(e1,e2);
        break;
      case LOEQ:
        jj_consume_token(LOEQ);
        e2 = Exp();
                     e1=new ASTLoeq(e1,e2);
        break;
      case HIGHER:
        jj_consume_token(HIGHER);
        e2 = Exp();
                       e1=new ASTHigher(e1,e2);
        break;
      case LESS:
        jj_consume_token(LESS);
        e2 = Exp();
                     e1=new ASTLess(e1,e2);
        break;
      default:
        jj_la1[5] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
    {if (true) return e1;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode Exp() throws ParseException {
  ASTNode e1, e2;
    e1 = Term();
    label_5:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PLUS:
      case MINUS:
        ;
        break;
      default:
        jj_la1[6] = jj_gen;
        break label_5;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PLUS:
        jj_consume_token(PLUS);
        e2 = Term();
                            e1 = new ASTAdd(e1,e2);
        break;
      case MINUS:
        jj_consume_token(MINUS);
        e2 = Term();
                             e1 = new ASTSub(e1,e2);
        break;
      default:
        jj_la1[7] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
       {if (true) return e1;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode Term() throws ParseException {
  ASTNode e1, e2;
    e1 = Fact();
    label_6:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case TIMES:
      case DIV:
        ;
        break;
      default:
        jj_la1[8] = jj_gen;
        break label_6;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case TIMES:
        jj_consume_token(TIMES);
        e2 = Fact();
                             e1 = new ASTMul(e1,e2);
        break;
      case DIV:
        jj_consume_token(DIV);
        e2 = Fact();
                           e1 = new ASTDiv(e1,e2);
        break;
      default:
        jj_la1[9] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
       {if (true) return e1;}
    throw new Error("Missing return statement in function");
  }

  final public ASTNode Fact() throws ParseException {
  Token x; ASTNode e, e1, e2;LinkedList<Binding> l=new LinkedList<Binding>();
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case Num:
      x = jj_consume_token(Num);
                {if (true) return new ASTNum(Integer.parseInt(x.image));}// o numero e a imagem do token

      break;
    case LPAR:
      jj_consume_token(LPAR);
      e = E();
      jj_consume_token(RPAR);
                           {if (true) return e;}
      break;
    case MINUS:
      jj_consume_token(MINUS);
      e = Fact();
                           {if (true) return new ASTMinusNum(e);}
      break;
    case TRUE:
      jj_consume_token(TRUE);
               {if (true) return new ASTBoolean(true);}
      break;
    case FALSE:
      jj_consume_token(FALSE);
               {if (true) return new ASTBoolean(false);}
      break;
    case NOT:
      jj_consume_token(NOT);
      e = Fact();
                    {if (true) return new ASTBooleanNot(e);}
      break;
    case Id:
      x = jj_consume_token(Id);
                     {if (true) return new ASTId(x.image);}
      break;
    case DECL:
      jj_consume_token(DECL);
      label_7:
      while (true) {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case Id:
          ;
          break;
        default:
          jj_la1[10] = jj_gen;
          break label_7;
        }
        x = jj_consume_token(Id);
        jj_consume_token(EQ);
        e1 = Exp();
                                                 l.add(new Binding(x.image,e1));
      }
      jj_consume_token(IN);
      e2 = Exp();
      jj_consume_token(END);
          {if (true) return new ASTDecl(l, e2);}
      break;
    default:
      jj_la1[11] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  /** Generated Token Manager. */
  public ParserTokenManager token_source;
  SimpleCharStream jj_input_stream;
  /** Current token. */
  public Token token;
  /** Next token. */
  public Token jj_nt;
  private int jj_ntk;
  private int jj_gen;
  final private int[] jj_la1 = new int[12];
  static private int[] jj_la1_0;
  static {
      jj_la1_init_0();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x80000,0x40000,0x24000,0x24000,0x618000,0x618000,0x60,0x60,0x180,0x180,0x8000000,0x8903250,};
   }

  /** Constructor with InputStream. */
  public Parser(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public Parser(java.io.InputStream stream, String encoding) {
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new ParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 12; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 12; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public Parser(java.io.Reader stream) {
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new ParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 12; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 12; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public Parser(ParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 12; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(ParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 12; i++) jj_la1[i] = -1;
  }

  private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  private int jj_ntk() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  private int[] jj_expentry;
  private int jj_kind = -1;

  /** Generate ParseException. */
  public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[28];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 12; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 28; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  final public void enable_tracing() {
  }

  /** Disable tracing. */
  final public void disable_tracing() {
  }

}

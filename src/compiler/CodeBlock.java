package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

public class CodeBlock {

	ArrayList<String> code;
	private int labelcounter = 0;

	public CodeBlock() {
		code = new ArrayList<String>(100);
	}

	public void emit_push(int n) {
		code.add("sipush " + n);
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}


	
	public void emit_loeq() {

		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmple " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");

	}
	public void emit_hieq() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpge " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}

	public void emit_higher() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpgt " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}
	public void emit_less() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmplt " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}
	public void emit_dif() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpne " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}
	public void emit_equal() {
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("if_icmpeq " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}
	public void emit_or() {
		code.add("ior");
	}

	public void emit_and() {
		code.add("iand");
	}

	public void emit_true(){// nao esta completamente correcto i think
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("ifne " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}
	public void emit_false(){
		String label1 = createLabel();
		String label2 = createLabel();
		code.add("ifeq " + label1);
		code.add("sipush 1");
		code.add("goto " + label2);
		code.add(label1 + ":");
		code.add("sipush 0");
		code.add(label2 + ":");
	}
	// falta o boolean
	
	void dumpHeader() {
		System.out.println(".class public Demo");
		System.out.println(".super java/lang/Object");
		System.out.println("");
		System.out.println(";");
		System.out.println("; standard initializer");
		System.out.println(".method public <init>()V");
		System.out.println("   aload_0");
		System.out.println("   invokenonvirtual java/lang/Object/<init>()V");
		System.out.println("   return");
		System.out.println(".end method");
		System.out.println("");
		System.out.println(".method public static main([Ljava/lang/String;)V");
		System.out.println("       ; set limits used by this method");
		System.out.println("       .limit locals 10");
		System.out.println("       .limit stack 256");
		System.out.println("");
		System.out.println("       ; setup local variables:");
		System.out.println("");
		System.out.println("       ;    1 - the PrintStream object held in java.lang.System.out");
		System.out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		System.out.println("");
		System.out.println("       ; place your bytecodes here");
		System.out.println("       ; START");
		System.out.println("");
	}

	void dumpFooter() {
		System.out.println("       ; END");
		System.out.println("");
		System.out.println("");
		System.out.println("       ; convert to String;");
		System.out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		System.out.println("       ; call println ");
		System.out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		System.out.println("");
		System.out.println("       return");
		System.out.println("");
		System.out.println(".end method");
	}

	void dumpCode() {
		for (String s : code)
			System.out.println("       " + s);
	}

	

	private String createLabel() {

		return "label" + labelcounter++;

	}
	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");		
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");		
		out.println("       return");
		out.println("");		
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("       "+s);

	}

	public void dump(String filename) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
	}
}
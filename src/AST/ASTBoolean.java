package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTBoolean implements ASTNode {

	boolean val;
	private int value;
	public ASTBoolean(boolean value) {
		val=value;
		
	}

	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException  {
		value= val? 1 : 0;
		return value;
	}

	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	@Override
	public void compile(CodeBlock code) {//posso fazer isto?
		if(value==1)
			code.emit_true();
		else if(value==0)
			code.emit_false();
	}


}

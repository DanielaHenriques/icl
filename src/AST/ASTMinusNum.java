package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTMinusNum implements ASTNode {

	ASTNode val;
	

	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return -val.eval(env);
	}

	public ASTMinusNum(ASTNode n) {
		val = n;
	}

	@Override
	public String toString() {
		return "-"+val.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		
		code.emit_push(-Integer.parseInt(val.toString()));
	}
}

package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTHieq implements ASTNode {

	ASTNode left, right;

	public ASTHieq(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException  {
		if(Integer.parseInt(left.toString())>=Integer.parseInt(right.toString())){
			return 1;
		}
		else return 0;
	}

	@Override
	public String toString() {
		return left.toString()+">="+right.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		code.emit_hieq();
	}


}

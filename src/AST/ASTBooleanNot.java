package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTBooleanNot implements ASTNode {

	ASTNode val;
	private int value;
	public ASTBooleanNot(ASTNode value) {
		val=value;
		
	}

	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException  {
		value= val.eval(env)==1? 0 : 1;
		
		return value;
	}

	@Override
	public String toString() {
		return val.toString();
		}

	@Override
	public void compile(CodeBlock code) {//posso fazer isto?
		if(value==1)
			code.emit_true();
		else if(value==0)
			code.emit_false();
	}


}

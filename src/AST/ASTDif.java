package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTDif implements ASTNode {

	ASTNode left, right;

	public ASTDif(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException  {
		if(!left.toString().equals(right.toString())){
			return 1;
		}
		else return 0;
	}

	@Override
	public String toString() {
		return left.toString()+"!="+right.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		code.emit_dif();
	}
}

package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTOr implements ASTNode {

	ASTNode left, right;

	public ASTOr(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		if(left.toString().equals("0") && right.toString().equals("0") )
			return 0;
			else
		return 1;
	}

	@Override
	public String toString() {
		return left.toString() + " || " + right.toString();
	}

	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		code.emit_or();
	}
}

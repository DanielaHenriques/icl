package tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import main.Console;
import parser.ParseException;

public class InterpreterTests {

	private void testCase(String expression, int value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	private void testNegativeCase(String expression, int value) throws ParseException {
		assertFalse(Console.acceptCompare(expression,value));
	}
	
	@Test
	public void test01() throws Exception {
		testCase("1\n",1);
	}
	
	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2\n",3);
		testCase("1-2-3\n",-4);
		testCase("4*2\n",8);
		testCase("4/2/2\n",1);
	}
	@Test
	public void testsLabClass04() throws Exception {
		testCase("decl x = decl y = 3 in 10*y end in x + decl y = 2 in x+y end end\n",62);
		testCase("decl x = 1 in decl x = 2 in x*5 end + x end\n", 11);
		testCase("decl x = 1 y = 2 in decl x = 3 z = 4 in x + y + z end end\n",9);
	}
}

# Lab classes for ICL 16/17

This repository contains the started code needed in the Lab classes of the ICL 16/17 course. Students should fork this repository into their private account on bitbucket, and use it as their own repository for developing the course assignments. Code will be added along the semester 

## Lab 5 - Compiling Identifier Declarations

Synchronize your repository to find new starter code with sample code for compiling declarations. 

Compile the language of Lab 4 according to the suggestions in ICL-4.pdf file (in clip).

## Lab 4 - Identifier Declarations

Synchronize your repository to find new starter code with single binder declaration expression. Implement the new declaration and identifier expressions as in the following grammar:

~~~
exp ::= 'decl' bindings 'in' exp 'end' | id
bindings ::= id '=' exp | id '=' exp bindings 
~~~

### Files

- `Environment.java`: The auxiliary class that implements the auxiliary data structure to maintain the evaluation environment for an expression.
- `ASTDecl.java`: The AST class implementation to represent a declaration of an identifier.
- `ASTId.java`: The AST class implementation to represent an bound occurrence of an indentifier.

## Lab 3 - Compiling to the JVM

Synchronize your repository to find new starter code for a small compiler. A compiler is just a new semantic function that takes the set of programs as its domain and the set of bytecode instruction sequences as target set.

### Files

- `Demo.j`: Your first jvm bytecode file
- `Compiler/CodeBlock.java`: The auxiliary class that will compute the results of the compilation
- `Main/Compiler.java`: The main file for the compiler that parses and compiles your code

### Compiler function

The compiler function is implemented through a method `void compile(CodeBlock code);` in every class of the AST.

### Use Jasmin to produce bytecode from the assembly language file

The following command line instruction (ran on the project directory) calls the compiler, accepts an expression and produces the corresponding assembly instructions.
 
~~~
java -cp bin/ Main.Compiler > a.j 
~~~

and this next one calls jasmin to produce bytecode

~~~
java -jar jasmin.jar a.j 
~~~

Try it on.

### Goal 1

Implement the `compile` method on every AST class (extend the given code to include the new expressions).

### Goal 2

Produce tests that actually use the compiler and the jvm to run the program an obtain a result from the compiled code. 

## Lab 2 - Evaluating an expression to Integer values

The definition of the AST was added to the provided code. An evaluation function is provided to define the structural operational semantics of the language. The testing infrastructure was also updated.

### Goal 1

Extend the evaluation function to all operators introduced in Lab 1

### Goal 2 

Produce enough tests to ensure a correct implementation

## Lab 1 - Parsing

The code in file `Parser.jj` implements the starting point for the following grammar, following the usual priority for the operators.

~~~
exp ::= exp '+' exp | exp '-' exp | exp '*' exp | exp '/' exp |  '(' exp ')' | num

num ::= ['1'-'9']['0'-'9']*
~~~

Test the given grammar, using the provided `JUNit` classes, and write more tests for it.

### Goal 1

Extend the grammar to have the following extra operations

~~~
exp ::= exp '+' exp | exp '-' exp | exp '*' exp | exp '+' exp | '-' exp | '(' exp ')' | num

cmp ::= exp '<' exp | exp '>' exp | exp '==' exp | exp '!=' exp | ...

bexp ::= bool | cmp | exp '&&' exp | exp '||' exp | '!' exp 

bool ::= true | false

num ::= ['1'-'9']['0'-'9']*
~~~

Extend the semantic actions to compute the values 

### Goal 2

Produce new tests that cover significant cases of the new grammar.
